﻿using System;

namespace Share
{
    public class Shared
    {

        public string isstartupregistered { get; set; }
        public string startupstage { get; set; }
        public string startuplocation { get; set; }
        public string startupname { get; set; }
        public string startupcity { get; set; }
        public string startupfunding { get; set; }
        public string howwasfundingreceived { get; set; }
        public string registeredyears { get; set; }
        public string intrestedin { get; set; }
        public string grossrev { get; set; }
        public string netrev { get; set; }
        public string startpitch { get; set; }
        public string applicantname { get; set; }
        public string applicantemail { get; set; }

        public string country { get; set; }
        public string webinfo { get; set; }

        public string size { get; set; }

        public string sector { get; set; }

        public string value { get; set; }
    }


    public class SME
  {
        public string smeupname { get; set; }
        public string smeplocation { get; set; }
        public string smecity { get; set; }
        public string smecountry { get; set; }
        public string smesize { get; set; }
        public string smesector { get; set; }
        public string smeregisteredyears { get; set; }
        public string smeintrestedin { get; set; }
        public string smestartpitch { get; set; }
        public string smewebinfo { get; set; }
        public string smegrossrev { get; set; }
        public string smenetrev { get; set; }

        public string smeapplicantname { get; set; }
        public string smeapplicantemail { get; set; }

 

    }


    public class IncubAcc
    {


        public string Incubator_Accelerator_stage { get; set; }
        public string Incubator_Accelerator_industrysector { get; set; }
        public string Incubator_Accelerator_orgname { get; set; }
        public string Incubator_Accelerator_cityId { get; set; }
        public string Incubator_Accelerator_countryId { get; set; }
        public string Incubator_Accelerator_stateId { get; set; }

        public string Incubator_Accelerator_priorities { get; set; }
        public string Incubator_Accelerator_orgpriorities { get; set; }
        public string Incubator_Accelerator_applicantname { get; set; }
        public string Incubator_Accelerator_applicantemail { get; set; }
        public string Incubator_Accelerator_webinfo { get; set; }



    }

    public class Bpartner
    {
        public string bdev_partner_size { get; set; }
        public string bdev_partner_name { get; set; }
        public string bdev_partner_sector { get; set; }
        public string bdev_partner_registeredyears { get; set; }
        public string bdev_partner_intrestedin { get; set; }
        public string bdev_partner_countryId { get; set; }
        public string bdev_partner_stateId { get; set; }
        public string bdev_partner_cityId { get; set; }
        public string bdev_partner_startpitch { get; set; }
        public string bdev_partner_webinfo { get; set; }
        public string bdev_partner_applicantname { get; set; }
        public string bdev_partner_applicantemail { get; set; }


    }


    public class Ipartner
    {

        public string Investor_partner_sector { get; set; }
        public string Investor_partner_cap { get; set; }
        public string Investor_partner_type { get; set; }
        public string Investor_partner_sector_exp { get; set; }
        public string Investor_partner_registeredyears { get; set; }
        public string Investor_partner_intrestedin { get; set; }
        public string Investor_partner_upname { get; set; }
        public string Investor_partner_stateId { get; set; }
        public string Investor_partner_countryId { get; set; }
        public string Investor_partner_cityId { get; set; }
        public string Investor_partner_webinfo { get; set; }
        public string Investor_partner_applicantname { get; set; }
        public string Investor_partner_applicantemail { get; set; }


    }

    public class Contact
    {

        public string contactname { get;set;}
             public string contactemail { get;set;}
public string contactphone { get;set;}
             public string contactmessage { get;set;}


    }

}
