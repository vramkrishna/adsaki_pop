﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;// Required for Using Sql  
using System.Configuration;
using Share;

namespace DATALAYER
{
    public class Adsakidal
    {

        public  void Post_Submit(Shared serviceobj)
        {

            try
            {
                
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Myconstr"].ToString());
                //  SqlCommand com = new SqlCommand("AddNewEmpDetails", con);
                SqlCommand com = new SqlCommand("StartupServiceDetails", con);
                com.CommandType = CommandType.StoredProcedure; 
                com.Parameters.AddWithValue("@isstartupregistered", serviceobj.isstartupregistered);
                com.Parameters.AddWithValue("@startupstage", String.IsNullOrEmpty(serviceobj.startupstage));
                com.Parameters.AddWithValue("@startuplocation", String.IsNullOrEmpty(serviceobj.startuplocation));
                com.Parameters.AddWithValue("@startupname", String.IsNullOrEmpty(serviceobj.startupname));
                com.Parameters.AddWithValue("@startupcity", String.IsNullOrEmpty(serviceobj.startupcity));
                com.Parameters.AddWithValue("@startupfunding", String.IsNullOrEmpty(serviceobj.startupfunding));
             
                 com.Parameters.AddWithValue("@howwasfundingreceived", String.IsNullOrEmpty(serviceobj.howwasfundingreceived));            
                com.Parameters.AddWithValue("@registeredyears", String.IsNullOrEmpty(serviceobj.registeredyears));
                com.Parameters.AddWithValue("@grossrev", String.IsNullOrEmpty(serviceobj.grossrev)); 
                com.Parameters.AddWithValue("@netrev", String.IsNullOrEmpty(serviceobj.netrev));
                com.Parameters.AddWithValue("@intrestedin", String.IsNullOrEmpty(serviceobj.intrestedin));
                com.Parameters.AddWithValue("@startpitch", String.IsNullOrEmpty(serviceobj.startpitch));
                com.Parameters.AddWithValue("@applicantname", String.IsNullOrEmpty(serviceobj.applicantname));
                com.Parameters.AddWithValue("@applicantemail", String.IsNullOrEmpty(serviceobj.applicantemail));
                com.Parameters.AddWithValue("@webinfo", serviceobj.webinfo);


                con.Open();
                int i = com.ExecuteNonQuery();
                con.Close();
               

            }
            catch(Exception ex)
            {
                throw ex;


            }

           




        }

        public void Post_Sme_Submit(SME smeserviceobj)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Myconstr"].ToString());
                //  SqlCommand com = new SqlCommand("AddNewEmpDetails", con);
                SqlCommand com = new SqlCommand("SME_Details", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@smeupname", String.IsNullOrEmpty(smeserviceobj.smeupname));
                com.Parameters.AddWithValue("@smeplocation", String.IsNullOrEmpty(smeserviceobj.smeplocation));
                com.Parameters.AddWithValue("@smecity", String.IsNullOrEmpty(smeserviceobj.smecity));
                com.Parameters.AddWithValue("@smecountry", String.IsNullOrEmpty(smeserviceobj.smecountry));
                com.Parameters.AddWithValue("@smesize", String.IsNullOrEmpty(smeserviceobj.smesize));
                com.Parameters.AddWithValue("@smesector", String.IsNullOrEmpty(smeserviceobj.smesector));

                com.Parameters.AddWithValue("@smeregisteredyears", String.IsNullOrEmpty(smeserviceobj.smeregisteredyears));
                com.Parameters.AddWithValue("@smeintrestedin", String.IsNullOrEmpty(smeserviceobj.smeintrestedin));
                com.Parameters.AddWithValue("@smestartpitch", String.IsNullOrEmpty(smeserviceobj.smestartpitch));
                com.Parameters.AddWithValue("@smewebinfo", String.IsNullOrEmpty(smeserviceobj.smewebinfo));
                com.Parameters.AddWithValue("@smegrossrev", String.IsNullOrEmpty(smeserviceobj.smegrossrev));
                com.Parameters.AddWithValue("@smenetrev", String.IsNullOrEmpty(smeserviceobj.smenetrev));
                com.Parameters.AddWithValue("@smeapplicantname", String.IsNullOrEmpty(smeserviceobj.smeapplicantname));
                com.Parameters.AddWithValue("@smeapplicantemail", String.IsNullOrEmpty(smeserviceobj.smeapplicantemail));


                con.Open();
                int i = com.ExecuteNonQuery();
                con.Close();


            }
            catch (Exception ex)
            {
                throw ex;

            }



        }


        public void Incubator_Accelerator_Email(IncubAcc incubacc)
        {

            try
            {

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Myconstr"].ToString());
                //  SqlCommand com = new SqlCommand("AddNewEmpDetails", con);
                SqlCommand com = new SqlCommand("Incubator_Accelerator_SP", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Incubator_Accelerator_stage", incubacc.Incubator_Accelerator_stage);
                com.Parameters.AddWithValue("@Incubator_Accelerator_industrysector", incubacc.Incubator_Accelerator_industrysector);
                com.Parameters.AddWithValue("@Incubator_Accelerator_orgname", String.IsNullOrEmpty(incubacc.Incubator_Accelerator_orgname));
                com.Parameters.AddWithValue("@Incubator_Accelerator_cityId", incubacc.Incubator_Accelerator_cityId);
                com.Parameters.AddWithValue("@Incubator_Accelerator_countryId", incubacc.Incubator_Accelerator_countryId);
                com.Parameters.AddWithValue("@Incubator_Accelerator_stateId", String.IsNullOrEmpty(incubacc.Incubator_Accelerator_stateId));

                com.Parameters.AddWithValue("@Incubator_Accelerator_priorities", String.IsNullOrEmpty(incubacc.Incubator_Accelerator_priorities));
                com.Parameters.AddWithValue("@Incubator_Accelerator_orgpriorities", String.IsNullOrEmpty(incubacc.Incubator_Accelerator_orgpriorities));
                com.Parameters.AddWithValue("@Incubator_Accelerator_applicantname", incubacc.Incubator_Accelerator_applicantname);
                com.Parameters.AddWithValue("@Incubator_Accelerator_applicantemail", incubacc.Incubator_Accelerator_applicantemail);
                com.Parameters.AddWithValue("@Incubator_Accelerator_webinfo", incubacc.Incubator_Accelerator_webinfo);
               
                con.Open();
                int i = com.ExecuteNonQuery();
                con.Close();


            }
            catch(Exception ex)
            {

                throw ex;
            }



        }



        public void BusinessPartnerEmail(Bpartner bpartner)
        {

            try
            {

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Myconstr"].ToString());
                //  SqlCommand com = new SqlCommand("AddNewEmpDetails", con);
                SqlCommand com = new SqlCommand("Business_Dev_SP", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@bdev_partner_size", bpartner.bdev_partner_size);
                com.Parameters.AddWithValue("@bdev_partner_name", String.IsNullOrEmpty(bpartner.bdev_partner_name));
                com.Parameters.AddWithValue("@bdev_partner_sector", String.IsNullOrEmpty(bpartner.bdev_partner_sector));
                com.Parameters.AddWithValue("@bdev_partner_registeredyears", String.IsNullOrEmpty(bpartner.bdev_partner_registeredyears));
                com.Parameters.AddWithValue("@bdev_partner_intrestedin", String.IsNullOrEmpty(bpartner.bdev_partner_intrestedin));
                com.Parameters.AddWithValue("@bdev_partner_countryId", String.IsNullOrEmpty(bpartner.bdev_partner_countryId));
                com.Parameters.AddWithValue("@bdev_partner_stateId", String.IsNullOrEmpty(bpartner.bdev_partner_stateId));
                com.Parameters.AddWithValue("@bdev_partner_cityId", String.IsNullOrEmpty(bpartner.bdev_partner_cityId));
                com.Parameters.AddWithValue("@bdev_partner_startpitch", String.IsNullOrEmpty(bpartner.bdev_partner_startpitch));
                com.Parameters.AddWithValue("@bdev_partner_webinfo", String.IsNullOrEmpty(bpartner.bdev_partner_webinfo));
                com.Parameters.AddWithValue("@bdev_partner_applicantname", String.IsNullOrEmpty(bpartner.bdev_partner_applicantname));
                com.Parameters.AddWithValue("@bdev_partner_applicantemail", String.IsNullOrEmpty(bpartner.bdev_partner_applicantemail));

                con.Open();
                int i = com.ExecuteNonQuery();
                con.Close();


            }
            catch (Exception ex)
            {

                throw ex;
            }


        }


        public void InvestorPartnerEmail(Ipartner ipartner)
        {

            try
            {

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Myconstr"].ToString());
                //  SqlCommand com = new SqlCommand("AddNewEmpDetails", con);
                SqlCommand com = new SqlCommand("Investor_Partner_SP", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Investor_partner_type", ipartner.Investor_partner_type);
                com.Parameters.AddWithValue("@Investor_partner_cap", ipartner.Investor_partner_cap);
                
               com.Parameters.AddWithValue("@Investor_partner_sector_exp", ipartner.Investor_partner_sector_exp);
                com.Parameters.AddWithValue("@Investor_partner_registeredyears", ipartner.Investor_partner_registeredyears);
                com.Parameters.AddWithValue("@Investor_partner_intrestedin", ipartner.Investor_partner_intrestedin);

                com.Parameters.AddWithValue("@Investor_partner_upname", ipartner.Investor_partner_upname);
                com.Parameters.AddWithValue("@Investor_partner_stateId", String.IsNullOrEmpty(ipartner.Investor_partner_stateId));
                com.Parameters.AddWithValue("@Investor_partner_countryId", ipartner.Investor_partner_countryId);
                com.Parameters.AddWithValue("@Investor_partner_cityId", ipartner.Investor_partner_cityId);
                com.Parameters.AddWithValue("@Investor_partner_webinfo", ipartner.Investor_partner_webinfo);
                com.Parameters.AddWithValue("@Investor_partner_applicantname", ipartner.Investor_partner_applicantname);
                com.Parameters.AddWithValue("@Investor_partner_applicantemail", ipartner.Investor_partner_applicantemail);

                con.Open();
                int i = com.ExecuteNonQuery();
                con.Close();


            }
            catch (Exception ex)
            {
                throw ex;

            }


        }




        public void ContactEmail(Contact contatcobj)
        {
            try
            {

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Myconstr"].ToString());
                //  SqlCommand com = new SqlCommand("AddNewEmpDetails", con);
                SqlCommand com = new SqlCommand("Contact_Adsaki_SP", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@contactname", contatcobj.contactname);
                com.Parameters.AddWithValue("@contactemail", contatcobj.contactemail);

                com.Parameters.AddWithValue("@contactphone", contatcobj.contactphone);
                com.Parameters.AddWithValue("@contactmessage", contatcobj.contactmessage);
                

                con.Open();
                int i = com.ExecuteNonQuery();
                con.Close();


            }
            catch (Exception ex)
            {
                throw ex;

            }



        }
    }
}
