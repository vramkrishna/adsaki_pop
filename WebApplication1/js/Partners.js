﻿$(document).ready(function () {



    $("#dropval").change(function () {
        var val = $(this).val();
        if (val === "Incubator_dropdown") {

            $("#Incubator_Accelerator").show();
            $("#incu_acc_para1").show();
            $("#Incubator_Accelerator_form").show();
            $("#btnhide").show();
            $("#bdev_para2").hide();
            $("#partner_para3").hide();
            $("#bdev-partner").hide();
            $("#bdev-partner_form").hide();
            $("#i-partner").hide();
            $("#Investor_partner_form").hide();
            $("#showprod").hide();

        }
        else if (val === "bdev-partner_dropdown") {
            $("#bdev-partner").show();
            $("#incu_acc_para1").hide();
            $("#partner_para3").hide();
            $("#bdev_para2").show();
            $("#bdev-partner_form").show();
            $("#btnhide").show();
            $("#Incubator_Accelerator").hide();
            $("#Incubator_Accelerator_form").hide();
            $("#i-partner").hide();
            $("#Investor_partner_form").hide();
            $("#showprod").hide();
        }

        else if (val == "i-partner_dropdown") {
            $("#i-partner").show();
            $("#Investor_partner_form").show();
            $("#partner_para3").show();

            $("#btnhide").show();
            $("#incu_acc_para1").hide();
            $("#bdev_para2").hide();
            $("#Incubator_Accelerator").hide();
            $("#Incubator_Accelerator_form").hide();
            $("#bdev-partner_form").hide();
            $("#bdev-partner").hide();
            $("#showprod").hide();

        }
        else if (val == "Select")
        {
            {
                $("#i-partner").hide();
                $("#Investor_partner_form").hide();
                $("#partner_para3").hide();

                $("#btnhide").hide();
                $("#incu_acc_para1").hide();
                $("#bdev_para2").hide();
                $("#Incubator_Accelerator").hide();
                $("#Incubator_Accelerator_form").hide();
                $("#bdev-partner_form").hide();
                $("#bdev-partner").hide();
                $("#showprod").hide();

            }

        }

    });


    //$("#Incubator_dropdown").on("click", function () {

    //    $("#Incubator_Accelerator").show();
    //    $("#Incubator_Accelerator_form").show();
    //    $("#btnhide").show();
    //    $("#bdev-partner").hide();
    //    $("#bdev-partner_form").hide();
    //    $("#i-partner").hide();
    //    $("#Investor_partner_form").hide();
    //    $("#showprod").hide();
    //});


    //$("#bdev-partner_dropdown").on("click", function () {

    //    $("#bdev-partner").show();
    //    $("#bdev-partner_form").show();
    //    $("#btnhide").show();
    //    $("#Incubator_Accelerator").hide();
    //    $("#Incubator_Accelerator_form").hide();
    //    $("#i-partner").hide();
    //    $("#Investor_partner_form").hide();
    //    $("#showprod").hide();


    //});
    //$("#i-partner_dropdown").on("click", function () {

    //    $("#i-partner").show();
    //    $("#Investor_partner_form").show();
    //    $("#btnhide").show();
    //    $("#Incubator_Accelerator").hide();
    //    $("#Incubator_Accelerator_form").hide();
    //    $("#bdev-partner_form").hide();
    //    $("#bdev-partner").hide();
    //    $("#showprod").hide();



    //});


    $('#btnSubmit1').click(function () {


        var valid = $("#f1").validationEngine('validate');

        if (valid == true) {
            $("#showwait").show();
            var email = new Object();


            email.Incubator_Accelerator_stage = $('#Incubator_Accelerator_stage option:selected').text();
            email.Incubator_Accelerator_industrysector = $('#Incubator_Accelerator_industrysector option:selected').text();

            email.Incubator_Accelerator_orgname = $('#Incubator_Accelerator_orgname').val();
            email.Incubator_Accelerator_cityId = $('#Incubator_Accelerator_cityId').val();
            email.Incubator_Accelerator_countryId = $('#Incubator_Accelerator_countryId').val();
            email.Incubator_Accelerator_stateId = "Null";

            email.Incubator_Accelerator_priorities = $('#Incubator_Accelerator_priorities').val();
            if ($('#Incubator_Accelerator_priorities').val() == "") {
                email.Incubator_Accelerator_priorities = "N/A";
            }

            email.Incubator_Accelerator_orgpriorities = $('#Incubator_Accelerator_orgpriorities').val();
            if ($('#Incubator_Accelerator_orgpriorities').val() == "") {
                email.Incubator_Accelerator_priorities = "N/A";
            }

            email.Incubator_Accelerator_applicantname = $('#Incubator_Accelerator_applicantname').val();
            email.Incubator_Accelerator_applicantemail = $('#Incubator_Accelerator_applicantemail').val();
            email.Incubator_Accelerator_webinfo = $('#Incubator_Accelerator_webinfo').val();



            if (email != null) {
                Email_Send("Incubator_Accelerator_Email", email);
            }

        }


        else {

            $("#f1").validationEngine();


        }




    });

    $('#btnSubmit2').click(function () {

        var valid = $("#f1").validationEngine('validate');

        if (valid == true) {
            $("#showwait").show();
            var email = new Object();
            email.bdev_partner_size = $('#bdev_partner_size option:selected').text();
            email.bdev_partner_name = $('#bdev_partner_name').val();
            email.bdev_partner_sector = $('#bdev_partner_sector option:selected').text();

            email.bdev_partner_registeredyears = $('#bdev_partner_registeredyears option:selected').text();
            email.bdev_partner_intrestedin = $('#bdev_partner_intrestedin option:selected').text();
            email.bdev_partner_countryId = $('#bdev_partner_countryId').val();
            email.bdev_partner_stateId = $('#bdev_partner_stateId').val();
            email.bdev_partner_cityId = $('#bdev_partner_cityId').val();
            email.bdev_partner_startpitch = $('#bdev_partner_startpitch').val();
            email.bdev_partner_webinfo = $('#bdev_partner_webinfo').val();
            email.bdev_partner_applicantname = $('#bdev_partner_applicantname').val();
            email.bdev_partner_applicantemail = $('#bdev_partner_applicantemail').val();


            if (email != null) {
                Email_Send("BusinessPartnerEmail", email);
            }
        }

        else {
            $("#f1").validationEngine();

        }

    });


    $('#btnSubmit3').click(function () {

        var valid = $("#f1").validationEngine('validate');

        if (valid == true) {
            debugger;
            $("#showwait").show();
            var email = new Object();
            email.Investor_partner_type = $('#Investor_partner_type option:selected').text();
            email.Investor_partner_cap = $('#Investor_partner_cap option:selected').val();
            email.Investor_partner_sector_exp = $('#Investor_partner_sector_exp option:selected').text();
            email.Investor_partner_registeredyears = $('#Investor_partner_registeredyears option:selected').text();
            email.Investor_partner_intrestedin = $('#Investor_partner_intrestedin option:selected').text();
            email.Investor_partner_upname = $('#Investor_partner_upname').val();
            email.Investor_partner_stateId = $('#Investor_partner_stateId').val();
            email.Investor_partner_countryId = $('#Investor_partner_countryId').val();
            email.Investor_partner_cityId = $('#Investor_partner_cityId').val();
            email.Investor_partner_webinfo = $('#Investor_partner_webinfo').val();
            email.Investor_partner_applicantname = $('#Investor_partner_applicantname').val();
            email.Investor_partner_applicantemail = $('#Investor_partner_applicantemail').val();


            if (email != null) {
                Email_Send("InvestorPartnerEmail", email);
            }
        }

        else {
            $("#f1").validationEngine();

        }

    });

    function Email_Send(myUrl, MyData) {

        try {

            $.ajax({
                type: "POST",
                url: "/Partners/" + myUrl,
                data: JSON.stringify(MyData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != null) {

                        $("#showwait").hide();
                        $('.form-control').val('');
                        $("#showprod").show();
                        //ShowAndCloseEmailSentPopoup(true);
                    }
                    else {
                        alert("unable to dispath mail..please try again.");
                    }

                    //showHideButton(false);
                },
                failure: function (response) {
                    alert("failed to send mail ..");
                },
                error: function (response) {
                    //showHideButton(false);
                    alert("please try after sometime.");
                }
            });


        }

        catch (e) {


            console.log(e);

        }



    }


});

