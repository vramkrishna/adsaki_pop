$(function () {
    var form = $("#signup-form");
    // validate signup form on keyup and submit
    form.validate({
        rules: {
            ship: {
                required: true
            },
            ownership: {
                required: true
            },
            quality: {
                required: true
            },
            experience: {
                required: true
            },
            city: {
                required: true
            },
            cityspecify: {
                required: true
            },
            brandname: {
                required: true
            },
            instagram: {
                required: true
            },
            firstname: {
                required: true
            },
            lastname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            chck: {
                required: true               
            },

        },
        messages: {
            ship: "Sorry, you need to be able to ship items!",
            ownership: "Sorry, no third party products!",
            quality: "Sorry, poor quality products are not acceptable!",
            experience: "Sorry,we require a five year minimum",
            city: "Please choose your city of residence or Other",
            cityspecify: "Please enter your city of residence",
            brandname: "Please enter your brand name",
            instagram: "Please enter your Instagram account name",
            firstname: "Please enter your first name",
            lastname: "Please enter your last name",
            chck:"Please indicate that you accept the product sample shipment requirement",
            email: {
                required: "Please enter your email address",
                email: "Please enter a valid email address"
            }
        }
    });

    $("#wizard").steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) {
                if (newIndex === 1) {
                    $('.wizard > .steps ul').addClass('step-2');
                } else {
                    $('.wizard > .steps ul').removeClass('step-2');
                }
                return true
            }

            if (newIndex !== 0 && form.valid()) {
                $("#header").text("Congrats! We can’t wait to have you launch your first campaign on PopTikr.")
            } else {
                $("#header").text("To maintain a healthy ecosystem, PopTikr requires our brands to submit an application for review and meet the following minimum requirements:")
            }

            if (form.valid()) {
                if (newIndex === 1) {
                    $('.wizard > .steps ul').addClass('step-2');
                } else {
                    $('.wizard > .steps ul').removeClass('step-2');
                }
            }

            return form.valid();
        },
        onFinished: function (event, currentIndex) {            
            if (form.valid()) {
                var city = $('#city').val()
                if (city === 'other') {
                    city = $('#cityspecify').val()
                }

                var brandPayload = {
                    business_name: $('#brandname').val(),
                    name: $('#firstname').val() + " " + $('#lastname').val(),
                    email: $('#email').val(),
                    instagram_account: $('#instagram').val(),
                    city: city,
                    "g-recaptcha-response": recaptchaResponse
                }

                $.post("../api/brands", brandPayload)
                    .done(function (data) {
                        if (currentIndex === 1) {
                            $('.wizard > .steps ul').addClass('step-3');
                        } else {
                            $('.wizard > .steps ul').removeClass('step-3');
                        }

                        $(".content").html('<p align="center">Thank you, we will review your application.<br>Look out for PopTikr email and our newsletter for updates on our exciting upcoming launch.<br> Cheers! <br>Your team at PopTikr</p>')
                        $(".actions").empty()
                    }).fail(function (err) {
                        if (err.responseText.includes("email")) {
                            $("#emaildiv").append('<label id="email-error" class="error" for="email">This email is already registered. Please pick another</label>');
                        } else if (err.responseText.includes("business_name")) {
                            $("#branddiv").append('<label id="brandname-error" class="error" for="brandname">This brand name is already registered. Please pick another</label>');
                        } else if (err.responseText.includes("captcha")) {
                            $("#branddiv").append('<label id="email-error" class="error" for="brandname">Please click the captcha below!</label>');
                        } else {
                            alert("Please contact site admin with this error: " + err.responseText)
                        }
                    })
            }

            return form.valid();
        },
        labels: {
            finish: "Submit",
            next: "Continue",
            previous: "Back"
        }
    });
    // Custom Button Jquery Steps
    $('.forward').click(function () {
        $("#wizard").steps('next');
    })
    $('.backward').click(function () {
        $("#wizard").steps('previous');
    })

    $('#city').on('change', function () {
        if (this.value === "other") {
            $('#cityspecifydiv').show()
        }
    });
    
    // Date Picker
    // var dp1 = $('#dp1').datepicker().data('datepicker');
    // dp1.selectDate(new Date());
})