$(function () {
    var form = $("#signup-form");
    // validate signup form on keyup and submit
    form.validate({
        rules: {
            size: {
                required: true
            },
            market: {
                required: true
            },
            instagram: {
                required: true
            },
            followers: {
                required: true,
                number: true
            },
            marketspecify: {
                required: true
            },
            demographic: {
                required: true
            },
            twitter: {
                url: true
            },
            facebook: {
                url: true
            },
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            size: "Sorry, this is required!",
            market: "Sorry, market is required!",
            instagram: "Sorry, Instagram account is required!",
            followers: {
                required: "Sorry, estimated number of followers is required!",
                number: "Sorry, only number is allowed"
            },
            marketspecify: "Sorry, please specify!",
            demographic: "Sorry, please specify!",
            email: {
                required: "Please enter your email address to accept",
                email: "Please enter a valid email address"
            }
        }
    });

    $("#wizard").steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) {
                if (newIndex === 1) {
                    $('.wizard > .steps ul').addClass('step-2');
                    $('.wizard > .steps ul').removeClass('step-3');
                } else {
                    $('.wizard > .steps ul').removeClass('step-2');
                    $('#marketspecifydiv').hide()
                    $('#demographicdiv').hide()
                }
                return true
            }

            if (newIndex !== 0 && form.valid()) {
                if ($('#size').val() == 'regular') {
                    $("#header").html("READY to roll! We are launching soon, sign-up now and be one of the first brand ambassadors to exclusively pilot and launch your very first brand campaign with PopTikr. <u>Not an influencer?</u> Not a problem - if you’re on social media and would brand sign-up too.")
                    $('#marketdiv').hide()
                    $('#demographicdiv').hide()
                } else {
                    $("#header").html("READY to roll! We are launching soon, sign-up now and be one of the first brand ambassadors to exclusively pilot and launch your very first brand campaign with PopTikr.")
                    $('#marketdiv').show()
                    $('#demographicdiv').show()
                }
            } else {
                $("#header").text("To maintain a healthy ecosystem, PopTikr requires all our users to submit this form for review.")
            }

            if (form.valid()) {
                if (newIndex === 1) {
                    $('.wizard > .steps ul').addClass('step-2');
                    $('.wizard > .steps ul').removeClass('step-3');
                } else if (newIndex === 2) {
                    $('.wizard > .steps ul').addClass('step-3');
                    $('.wizard > .steps ul').removeClass('step-2');
                } else {
                    $('.wizard > .steps ul').removeClass('step-3');
                    $('.wizard > .steps ul').removeClass('step-2');
                }
            }

            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            if (form.valid()) {
                var city = $('#city').val()
                if (city === 'other') {
                    city = $('#cityspecify').val()
                }
                var market = $('#market').val()
                if (market === 'other') {
                    market = $('#marketspecify').val()
                }

                var influencerPayload = {
                    size: $('#size').val(),
                    num_followers: $('#followers').val(),
                    email: $('#email').val(),
                    market: market,
                    demographic: $('#demographic').val(),
                    twitter_account: $('#twitter').val(),
                    facebook_account: $('#facebook').val(),
                    instagram_account: $('#instagram').val(),
                    "g-recaptcha-response": recaptchaResponse
                }

                $.post("../api/influencers", influencerPayload)
                    .done(function (data) {
                        if (currentIndex === 2) {
                            $('.wizard > .steps ul').addClass('step-3');
                        } else {
                            $('.wizard > .steps ul').removeClass('step-3');
                        }

                        $(".content").html('<p align="center">Thank you, we will review your application.<br>Look out for PopTikr email and our newsletter for updates on our exciting upcoming launch.<br> Cheers! <br>Your team at PopTikr</p>')
                        $(".actions").empty()
                    }).fail(function (err) {
                        if (err.responseText.includes("email")) {
                            $("#emaildiv").append('<label id="email-error" class="error" for="email">This email is already taken. Please pick another</label>');
                        } else if (err.responseText.includes("instagram_account")) {
                            $("#instagramdiv").append('<label id="instagram-error" class="error" for="instagram">This Instagram account is already taken. Please pick another.</label>');
                        } else if (err.responseText.includes("captcha")) {
                            $("#emaildiv").append('<label id="email-error" class="error" for="email">Please click the captcha below!</label>');
                        } else {
                            alert("Please contact site admin with this error: " + err.responseText)
                        }
                    })
            }

            return form.valid();
        },
        labels: {
            finish: "Submit",
            next: "Continue",
            previous: "Back"
        }
    });
    // Custom Button Jquery Steps
    $('.forward').click(function () {
        $("#wizard").steps('next');
    })
    $('.backward').click(function () {
        $("#wizard").steps('previous');
    })

    $('#market').on('change', function () {
        if (this.value === "other") {
            $('#marketspecifydiv').show()
        }

    });

    $('#city').on('change', function () {
        if (this.value === "other") {
            $('#cityspecifydiv').show()
        }
    });
    // Date Picker
    // var dp1 = $('#dp1').datepicker().data('datepicker');
    // dp1.selectDate(new Date());
})