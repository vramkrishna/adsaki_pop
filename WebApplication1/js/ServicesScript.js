﻿$(document).ready(function () {



    $("#dropval").change(function () {
        var val = $(this).val();
        if (val === "start_dropdown") {

            $("#stratup").show();
            $("#dsah_start").show();
            $("#stratup_form").show();
            $("#btnhide").show();
            $("#dsah_sme").hide();
            $("#sme").hide();
            $("#sme_form").hide();
            $("#showprod").hide();

        }
        else if (val === "sme_dropdown") {
            $("#sme").show();
            $("#sme_form").show();
            $("#btnhide").show();
            $("#dsah_sme").show();
            $("#stratup").hide();
            $("#dsah_start").hide();
            $("#stratup").hide();
            $("#stratup_form").hide();
            $("#showprod").hide();
        }
        else if (val == "Select")
        {
            $("#sme").hide();
            $("#sme_form").hide();
            $("#btnhide").hide();
            $("#dsah_sme").hide();
            $("#stratup").hide();
            $("#dsah_start").hide();
            $("#stratup").hide();
            $("#stratup_form").hide();
            $("#showprod").hide();
        }

    });


    //$("#start_dropdown").on("click", function () {

    //    $("#stratup").show();
    //    $("#stratup_form").show();
    //    $("#btnhide").show();
    //    $("#sme").hide();
    //    $("#sme_form").hide();
    //    $("#showprod").hide();

    //});


    //$("#sme_dropdown").on("click", function () {

    //    $("#sme").show();
    //    $("#sme_form").show();
    //    $("#btnhide").show();
    //    $("#stratup").hide();
    //    $("#stratup").hide();
    //    $("#stratup_form").hide();
    //    $("#showprod").hide();

    //});


    $('#btnSubmit1').click(function () {


        var valid = $("#f1").validationEngine('validate');

        if (valid == true) {
            $("#showwait").show();
            var email = new Object();


            email.isstartupregistered = $('#isstartupregistered option:selected').text();
            email.startupstage = $('#startupstage option:selected').text();

            email.startuplocation = $('#startuplocation').val();
            email.startupname = $('#startupname').val();
            email.startupcity = $('#startupcity').val();
            email.startupfunding = $('#startupfunding option:selected').text();
            email.howwasfundingreceived = $('#howwasfundingreceived option:selected').text();
            email.registeredyears = $('#registeredyears option:selected').text();
            email.intrestedin = $('#intrestedin option:selected').text();
            email.grossrev = $('#grossrev').val();
            if ($('#grossrev').val() == "") {
                email.grossrev = "N/A";
            }


            email.netrev = $('#netrev').val();
            if ($('#netrev').val() == "") {
                email.netrev = "N/A";
            }



            email.startpitch = $('#startpitch').val();
            email.applicantname = $('#applicantname').val();
            email.applicantemail = $('#applicantemail').val();
            email.webinfo = $('#webinfo').val();
            email.size = $('#size option:selected').text();
            if ($('#size option:selected').text() == "") {
                email.size = "N/A";
            }

            email.sector = $('#sector option:selected').text();
            if ($('#sector option:selected').text() == "") {
                email.sector = "N/A";
            }


            email.country = $('#country').val();
            if ($('#country').val() == "") {
                email.country = "N/A";
            }


            if (email != null) {
                Email_Send("ServiceEmail", email);
            }

        }


        else {

            $("#f1").validationEngine();


        }




    });

    $('#btnSubmit2').click(function () {

        var valid = $("#f1").validationEngine('validate');

        if (valid == true) {

            $("#showwait").show();
            var email = new Object();
            email.smeupname = $('#smeupname').val();
            email.smeplocation = "null";
            email.smecity = $('#smecity').val();

            email.smeregisteredyears = $('#smeregisteredyears option:selected').text();
            email.smeintrestedin = $('#smeintrestedin option:selected').text();
            email.smesize = $('#smesize option:selected').text();
            email.smesector = $('#smesector option:selected').text();
            email.smecountry = $('#smecountry').val();
            email.smegrossrev = $('#smegrossrev').val();

            if ($('#smegrossrev').val() == "") {
                email.smegrossrev = "N/A";
            }


            email.smenetrev = $('#smenetrev').val();
            if ($('#smenetrev').val() == "") {
                email.smenetrev = "N/A";
            }


            email.smestartpitch = $('#smestartpitch').val();
            email.smeapplicantname = $('#smeapplicantname').val();
            email.smeapplicantemail = $('#smeapplicantemail').val();
            email.smewebinfo = $('#smewebinfo').val();
            if (email != null) {
                Email_Send("SmeEmail", email);
            }
        }

        else {
            $("#f1").validationEngine();

        }

    });

    function Email_Send(myUrl, MyData) {

        try {

            $.ajax({
                type: "POST",
                url: "/Home/" + myUrl,
                data: JSON.stringify(MyData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != null) {

                        $("#showwait").hide();
                        $('.form-control').val('');
                        $("#showprod").show();
                        //ShowAndCloseEmailSentPopoup(true);
                    }
                    else {
                        alert("unable to dispath mail..please try again.");
                    }

                    //showHideButton(false);
                },
                failure: function (response) {
                    alert("failed to send mail ..");
                },
                error: function (response) {
                    //showHideButton(false);
                    alert("please try after sometime.");
                }
            });


        }

        catch (e) {


            console.log(e);

        }



    }


});