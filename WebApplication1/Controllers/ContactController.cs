﻿using Share;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using BAL;
using System.Net;

namespace WebApplication1.Controllers
{
    public class ContactController : Controller
    {
        // GET: Contact
        [HttpPost]
        public ActionResult ContactEmail(Contact contactobj)
        {
            Adsakibal Adsakibalobj = new Adsakibal();
            try
            {




                string Subject = "CONTACT ENQUIRY";
                //string body = Adsakibalobj.createEmailBodyProd(serviceobj);
                Adsakibalobj.ContactEmail(contactobj);
                string body = this.createEmailBodyContact(contactobj);
                string sendmail = this.SendEmail(Subject, body);
                string clientbody = this.createEmailBodyClient();
                string ackemil = this.SendEmailClient(clientbody, contactobj.contactemail);

                return Json(contactobj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;

            }

        }




        private String SendEmailClient(string clientbody, string clientemail)
        {

            string sendmail = string.Empty;
            string CLientTo = clientemail; //sendig email to the support team
            string From = ConfigurationManager.AppSettings["From"];
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(CLientTo));
            email.Subject = "THANK YOU";
            email.Body = clientbody;
            email.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Credentials = new NetworkCredential("info@adsaki.com", "MiamiViceh8n");
            smtp.Port = 25;

            smtp.Send(email);
            sendmail = "Mail Sent";
            return sendmail;



            //SmtpClient smtp = new SmtpClient();
            //smtp.Timeout = 1000;
            //smtp.Host = "relay-hosting.secureserver.net";
            //smtp.Port = 25;
            //smtp.EnableSsl = true;
            //smtp.UseDefaultCredentials = false;
            //smtp.Credentials = new NetworkCredential("info@adsaki.com", "MiamiViceh8n");
            //smtp.Send(email);


            //sendmail = "Mail Sent";
            //return sendmail;

        }



     

        private String SendEmail(string Subject, string body)
        {

            string sendmail = string.Empty;
            string AdminTo = ConfigurationManager.AppSettings["To"];//sendig email to the support team
           string From = ConfigurationManager.AppSettings["From"];
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(AdminTo));
            email.Subject = Subject;
            email.Body = body;


            email.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Send(email);

            smtp.Timeout = 1000;
            sendmail = "Mail Sent";
            return sendmail;
        }

        private string createEmailBodyClient()
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/ackmail.html")))
            {
                body = reader.ReadToEnd();
            }

            return body;
        }
        private string createEmailBodyContact(Contact contactobj)
        {

            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/ContactEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{contactname}", contactobj.contactname);
            body = body.Replace("{contactemail}", contactobj.contactemail);
            body = body.Replace("{contactphone}", contactobj.contactphone);
            body = body.Replace("{contactmessage}", contactobj.contactmessage);
           


            return body;


        }

    }
}