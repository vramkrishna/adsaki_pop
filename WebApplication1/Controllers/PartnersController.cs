﻿using Share;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using BAL;

namespace WebApplication1.Controllers
{
    public class PartnersController : Controller
    {
        // GET: Partners
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]

        public ActionResult Incubator_Accelerator_Email(IncubAcc incubacc)
        {

            Adsakibal Adsakibalobj = new Adsakibal();
            Adsakibalobj.Incubator_Accelerator_Email(incubacc);
            string Subject = "I’m an Incubator/Accelerator- become a partne";
            string body = this.createEmailBodyIncubator_Accelerator_Email(incubacc);
            string sendmail = this.SendEmail(Subject, body);
            string clientbody = this.createEmailBodyClient();
            string ackemil = this.SendEmailClient(clientbody, incubacc.Incubator_Accelerator_applicantemail);
            return Json(incubacc, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult BusinessPartnerEmail(Bpartner bpartner)
        {
            Adsakibal Adsakibalobj = new Adsakibal();
            Adsakibalobj.BusinessPartnerEmail(bpartner);
            string Subject = "I’m a Business Development Firm- become a partner";
            string body = this.createEmailBodyBusinessPartnerEmail(bpartner);
            string sendmail = this.SendEmail(Subject, body);
            string clientbody = this.createEmailBodyClient();
            string ackemil = this.SendEmailClient(clientbody, bpartner.bdev_partner_applicantemail);
            return Json(bpartner, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult InvestorPartnerEmail(Ipartner ipartner)
        {
            Adsakibal Adsakibalobj = new Adsakibal();
            Adsakibalobj.InvestorPartnerEmail(ipartner);
            string Subject = "I’m an Investor- become a partner";
            string body = this.createEmailBodyInvestorPartnerEmail(ipartner);
            string sendmail = this.SendEmail(Subject, body);
            string clientbody = this.createEmailBodyClient();
            string ackemil = this.SendEmailClient(clientbody, ipartner.Investor_partner_applicantemail);
            return Json(ipartner, JsonRequestBehavior.AllowGet);
        }


        // for Incubator_Accelerator_Email render email template
        private string createEmailBodyIncubator_Accelerator_Email(IncubAcc incubacc)
        {

            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/Incubator_Accelerator.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Incubator_Accelerator_stage}", incubacc.Incubator_Accelerator_stage);
            body = body.Replace("{Incubator_Accelerator_industrysector}", incubacc.Incubator_Accelerator_industrysector);
            body = body.Replace("{Incubator_Accelerator_orgname}", incubacc.Incubator_Accelerator_orgname);
            body = body.Replace("{Incubator_Accelerator_cityId}", incubacc.Incubator_Accelerator_cityId);
            body = body.Replace("{Incubator_Accelerator_countryId}", incubacc.Incubator_Accelerator_countryId);
            body = body.Replace("{Incubator_Accelerator_stateId}", incubacc.Incubator_Accelerator_stateId);
            body = body.Replace("{Incubator_Accelerator_priorities}", incubacc.Incubator_Accelerator_priorities);
            body = body.Replace("{Incubator_Accelerator_orgpriorities}", incubacc.Incubator_Accelerator_orgpriorities);
            body = body.Replace("{Incubator_Accelerator_applicantname}", incubacc.Incubator_Accelerator_applicantname);
            body = body.Replace("{Incubator_Accelerator_applicantemail}", incubacc.Incubator_Accelerator_applicantemail);
            body = body.Replace("{Incubator_Accelerator_webinfo}", incubacc.Incubator_Accelerator_webinfo);
           


            return body;


        }




        // for BusinessPartnerEmail render email template
        private string createEmailBodyBusinessPartnerEmail(Bpartner bpartner)
        {

            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/business_dev.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{bdev_partner_size}", bpartner.bdev_partner_size);
            body = body.Replace("{bdev_partner_name}", bpartner.bdev_partner_name);
            body = body.Replace("{bdev_partner_sector}", bpartner.bdev_partner_sector);
            body = body.Replace("{bdev_partner_registeredyears}", bpartner.bdev_partner_registeredyears);
            body = body.Replace("{bdev_partner_intrestedin}", bpartner.bdev_partner_intrestedin);
            body = body.Replace("{bdev_partner_countryId}", bpartner.bdev_partner_countryId);
            body = body.Replace("{bdev_partner_stateId}", bpartner.bdev_partner_stateId);
            body = body.Replace("{bdev_partner_cityId}", bpartner.bdev_partner_cityId);
            body = body.Replace("{bdev_partner_startpitch}", bpartner.bdev_partner_startpitch);
            body = body.Replace("{bdev_partner_webinfo}", bpartner.bdev_partner_webinfo);
            body = body.Replace("{bdev_partner_applicantname}", bpartner.bdev_partner_applicantname);
            body = body.Replace("{bdev_partner_applicantemail}", bpartner.bdev_partner_applicantemail);



            return body;


        }






        //for InvestorPartnerEmail render email template
        private string createEmailBodyInvestorPartnerEmail(Ipartner ipartner)
        {

            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/investor_ partner.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Investor_partner_type}", ipartner.Investor_partner_type);
            body = body.Replace("{Investor_partner_cap}", ipartner.Investor_partner_cap);
            body = body.Replace("{Investor_partner_sector_exp}", ipartner.Investor_partner_sector_exp);
            body = body.Replace("{Investor_partner_registeredyears}", ipartner.Investor_partner_registeredyears);
            body = body.Replace("{Investor_partner_intrestedin}", ipartner.Investor_partner_intrestedin);
            body = body.Replace("{Investor_partner_upname}", ipartner.Investor_partner_upname);
            body = body.Replace("{Investor_partner_stateId}", ipartner.Investor_partner_stateId);
            body = body.Replace("{Investor_partner_countryId}", ipartner.Investor_partner_countryId);
            body = body.Replace("{Investor_partner_cityId}", ipartner.Investor_partner_cityId);
            body = body.Replace("{Investor_partner_webinfo}", ipartner.Investor_partner_webinfo);
            body = body.Replace("{Investor_partner_applicantname}", ipartner.Investor_partner_applicantname);
            body = body.Replace("{Investor_partner_applicantemail}", ipartner.Investor_partner_applicantemail);



            return body;


        }










        //for client ack body render email
        private string createEmailBodyClient()
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/ackmail.html")))
            {
                body = reader.ReadToEnd();
            }

            return body;
        }

        //for sending emails
        private String SendEmail(string Subject, string body)
        {

            string sendmail = string.Empty;
            string AdminTo = ConfigurationManager.AppSettings["To"]; //sendig email to the support team
            string From = ConfigurationManager.AppSettings["From"];
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(AdminTo));
            email.Subject = Subject;
            email.Body = body;
           

            email.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Send(email);

            smtp.Timeout = 1000;
            sendmail = "Mail Sent";
            return sendmail;

        }


        private String SendEmailClient(string clientbody, string clientemail)
        {

            string sendmail = string.Empty;
            string CLientTo = clientemail; //sendig email to the support team
            string From = ConfigurationManager.AppSettings["From"];
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(CLientTo));
            email.Subject = "THANK YOU";
            email.Body = clientbody;
            email.IsBodyHtml = true;
            //SmtpClient smtp = new SmtpClient();
            //smtp.UseDefaultCredentials = false;
            //smtp.Credentials = new System.Net.NetworkCredential("vishalramkrishna@cmail.carleton.ca", "Vis123_");
            //smtp.Port = 587; // You can use Port 25 if 587 is blocked
            //smtp.Host = "smtp-mail.outlook.com";
            //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            //smtp.EnableSsl = true;
            email.To.Add(new MailAddress(CLientTo));
            email.Subject = "THANK YOU";
            email.Body = clientbody;
            email.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Send(email);
            smtp.Timeout = 1000;
            sendmail = "Mail Sent";
            return sendmail;




            smtp.Send(email);

            smtp.Timeout = 50;
            sendmail = "Mail Sent";
            return sendmail;


        }







    }
}