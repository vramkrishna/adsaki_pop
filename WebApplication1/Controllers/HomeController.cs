﻿using Share;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using BAL;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [HttpPost]
        public ActionResult ServiceEmail(Shared serviceobj)
        {

            Adsakibal Adsakibalobj = new Adsakibal();

            try
            {



                 
                string Subject = "STARTUP ENQUIRY";
                //string body = Adsakibalobj.createEmailBodyProd(serviceobj);
                Adsakibalobj.Post_Submit(serviceobj);
                string body = this.createEmailBodyProd(serviceobj);
                string sendmail = this.SendEmail(Subject, body);
                string clientbody = this.createEmailBodyClient();
                string ackemil = this.SendEmailClient(clientbody, serviceobj.applicantemail);

                return Json(serviceobj, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {

                throw ex;

            }
          
        }


        public ActionResult SmeEmail(SME smeserviceobj)
        {
            Adsakibal Adsakibalobj = new Adsakibal();
            try
            {
                string Subject = "SME ENQUIRY";
                Adsakibalobj.Post_Sme_Submit(smeserviceobj);                
                string body = this.createEmailBodySME(smeserviceobj);
                string sendmail = this.SendEmail(Subject, body);
                string clientbody = this.createEmailBodyClient();
                string ackemil = this.SendEmailClient(clientbody, smeserviceobj.smeapplicantemail);
                return Json(smeserviceobj, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {

                throw ex;
            }
          

        }

        private string createEmailBodyProd(Shared serviceobj)
        {

            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/ServiceEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{isstartupregistered}", serviceobj.isstartupregistered);
            body = body.Replace("{startupstage}", serviceobj.startupstage);
            body = body.Replace("{startuplocation}", serviceobj.startuplocation);
            body = body.Replace("{startupname}", serviceobj.startupname);
            body = body.Replace("{startupcity}", serviceobj.startupcity);
            body = body.Replace("{startupfunding}", serviceobj.startupfunding);
            body = body.Replace("{howwasfundingreceived}", serviceobj.howwasfundingreceived);
            body = body.Replace("{registeredyears}", serviceobj.registeredyears);
            body = body.Replace("{startpitch}", serviceobj.startpitch);
            body = body.Replace("{intrestedin}", serviceobj.intrestedin);
            body = body.Replace("{grossrev}", serviceobj.grossrev);
            body = body.Replace("{netrev}", serviceobj.netrev);
            body = body.Replace("{applicantname}", serviceobj.applicantname);
            body = body.Replace("{applicantemail}", serviceobj.applicantemail);
            body = body.Replace("{webinfo}", serviceobj.webinfo);


            return body;


        }


        private string createEmailBodySME(SME smeserviceobj)
        {


            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/ServiceSmeEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            
            body = body.Replace("{smeupname}", smeserviceobj.smeupname);
            body = body.Replace("{smeplocation}", smeserviceobj.smeplocation);
            body = body.Replace("{smecity}", smeserviceobj.smecity);
            body = body.Replace("{smecountry}", smeserviceobj.smecountry);
            body = body.Replace("{smesize}", smeserviceobj.smesize);
            body = body.Replace("{smesector}", smeserviceobj.smesector);
            body = body.Replace("{smeregisteredyears}", smeserviceobj.smeregisteredyears);
            body = body.Replace("{smeintrestedin}", smeserviceobj.smeintrestedin);
            body = body.Replace("{smestartpitch}", smeserviceobj.smestartpitch);
            body = body.Replace("{smewebinfo}", smeserviceobj.smewebinfo);
            body = body.Replace("{smegrossrev}", smeserviceobj.smegrossrev);
            body = body.Replace("{smenetrev}", smeserviceobj.smenetrev);
            body = body.Replace("{smeapplicantname}", smeserviceobj.smeapplicantname);
            body = body.Replace("{smeapplicantemail}", smeserviceobj.smeapplicantemail);
        


            return body;
        }


        private string createEmailBodyClient()
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplate/ackmail.html")))
            {
                body = reader.ReadToEnd();
            }

            return body;
        }

        private String SendEmail(string Subject, string body)
        {

            string sendmail = string.Empty;
            string AdminTo = ConfigurationManager.AppSettings["To"];//sendig email to the support team
            string From = ConfigurationManager.AppSettings["From"];
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(AdminTo));
            email.Subject = Subject;
            email.Body = body;
            email.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Send(email);

            smtp.Timeout = 1000;
            sendmail = "Mail Sent";
            return sendmail;
        }


        private String SendEmailClient(string clientbody , string clientemail)
        {

            string sendmail = string.Empty;
            string CLientTo = clientemail; //sendig email to the support team
            string From = ConfigurationManager.AppSettings["From"];
            MailMessage email = new MailMessage();
            email.To.Add(new MailAddress(CLientTo));
            email.Subject = "THANK YOU";
            email.Body = clientbody;
            email.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Send(email);
            smtp.Timeout = 1000;
            sendmail = "Mail Sent";
            return sendmail;


        }


        }
}